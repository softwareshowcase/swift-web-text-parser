import Foundation

struct webParagraph {
    var text: String
    var links: [String]
    var media: [String]
}
