# Swift Web Article Text Parser

This Swift demo demonstrates how to load a web article or any primarily text-based web page and organize the text into its component paragraphs.

## Operation

Calling loadWebsite() with a URL and completion handler will load the webService object with webParagraph structures that contain the text of the article. The parseWebPage() method employs SwiftSoup, so you'll need to set that dependency up in your application. Some additional features that have yet to be implemented include grabbing any embedded links and images in and inbetween the text.
