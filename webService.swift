import Foundation
import SwiftSoup

class webService {
    var webURL: String = ""
    var webParagraphs: [webParagraph] = []
    
    func fetchWebPage(webLink: String, _ complete: @escaping (String) -> Void){
        guard let url = URL(string: webLink) else { complete(""); return }
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let dataString = String(data: data, encoding: .utf8) ?? ""
            complete(dataString)
        }
        task.resume()
    }
    
    func parseWebPage(webString: String, _ complete: @escaping (Bool) -> Void){
        do {
            let doc: Document = try SwiftSoup.parse(webString)
            let p : Elements = try! doc.select("p")
            let doc2: Document = try SwiftSoup.parseBodyFragment(webString)
            let headerTitle = try doc2.title()
            let text = try doc2.text()
            let body = doc2.body()
            for j in p{
                let paragraph: String = try j.text(trimAndNormaliseWhitespace: true)
                if paragraph != ""{
                    createWebParagraph(paragraphIn: paragraph)
                }
                else{
                    print("empty paragraph")
                }
            }
            webParagraphs.insert(webParagraph(text: headerTitle, links: [], media: []), at: 0)
            complete(true)
        }catch Exception.Error(_, let message) {
            print(message)
            complete(false)
        } catch {
            print("error")
            complete(false)
        }
    }
    
    func loadWebsite(webLink: String, _ complete: @escaping (Bool) -> Void){
        webParagraphs = []
        fetchWebPage(webLink: webLink){webString in
            if webString == ""{
                complete(false)
            }
            else{
                self.parseWebPage(webString: webString){parsed in
                    if parsed{
                        if self.webParagraphs.isEmpty{
                            complete(false)
                        }
                        else{
                            complete(true)
                            self.webURL = webLink
                        }
                    }
                }
            }
        }
    }
    
    func createWebParagraph(paragraphIn: String){
        webParagraphs.append(webParagraph(text: paragraphIn,
                                          links: [],
                                          media: []))
    }
    
    func grabLinksInParagraph(){
        
    }
    
    func grabMediaInParagraph(){
        
    }
}
